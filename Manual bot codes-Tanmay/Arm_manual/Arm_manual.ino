#include <PS2X_lib.h>
#include <Servo.h>
#include <Stepper.h>

PS2X ps2x;

Servo claw,wrist,elbow1,elbow2;

int data = 45;//17
int clk = 51;//46
int cmd = 47;//50
int att = 49;//48

int elbow_pos=0; 
int wrist_pos=0;
int claw_pos=0;
int ry,rx;

const int stepsPerRevolution=800;
Stepper base_stepper(stepsPerRevolution, 26, 28);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  ps2x.config_gamepad(clk, cmd, att, data, false, false);
  
  
  claw.attach(22);//7
  wrist.attach(24);//6
  elbow1.attach(21);//8
  elbow2.attach(20);//9

}

void loop() {
  // put your main code here, to run repeatedly:
  ps2x.read_gamepad();
 // ry =  ps2x.Analog(PSS_RY); //Right Stick Up and Down
 // rx =  ps2x.Analog(PSS_RX); //Right Stick Left and Right

  if(ps2x.ButtonPressed(PSB_TRIANGLE))
  {
    elbow_pos-=5;
    elbow_pos=constrain(elbow_pos,0,180);
    elbow1.write(elbow_pos);
    elbow2.write(180-elbow_pos);
  }
  if(ps2x.ButtonPressed(PSB_CROSS))
  {
    elbow_pos+=5;
    elbow_pos=constrain(elbow_pos,0,180);
    elbow1.write(elbow_pos);
    elbow2.write(180-elbow_pos);
  }
 if(ps2x.ButtonPressed(PSB_PAD_RIGHT))
  {
    base_stepper.step(200);
  }
   if(ps2x.ButtonPressed(PSB_PAD_LEFT))
  {
    base_stepper.step(-200);
  }
  if(ps2x.ButtonPressed(PSB_PAD_UP))
  {
    wrist_pos-=5;
    wrist_pos=constrain(wrist_pos,0,180);
    wrist.write(wrist_pos);
  }
  if(ps2x.ButtonPressed(PSB_PAD_DOWN))
  {
     wrist_pos+=5;
    wrist_pos=constrain(wrist_pos,0,180);
    wrist.write(wrist_pos);
  }
 
  
 if(ps2x.ButtonPressed(PSB_CIRCLE))
 {
  if(claw_pos==0)
  {
    claw_pos=180;
  }
  else if(claw_pos==180)
  {
    claw_pos=0;
  }
  claw.write(claw_pos); 
}
 Serial.print(wrist_pos);
  Serial.print("      ");
  Serial.print(elbow_pos);
 Serial.print("      ");
  Serial.println(claw_pos); 
}
