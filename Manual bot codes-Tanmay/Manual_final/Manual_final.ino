#include <PS2X_lib.h>
#include <Servo.h>
#include <Stepper.h>

PS2X ps2x;

Servo claw;
Servo rack1,rack2;

#define baseStepperP1 21
#define baseStepperP2 20
#define clawStepperP1 47
#define clawStepperP2 49
#define rackServo1 22
#define rackServo2 24
#define PS2_data 17
#define PS2_clk 46
#define PS2_cmd 50
#define PS2_att 48 
#define clawStepperSteps 1000
#define baseStepperSteps 800

int M1d1 = 23, M1d2 = 25, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M2d1 = 27, M2d2 = 29, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M3d1 = 37, M3d2 = 35, M3pwm = 2; //M3 is the thirdmotor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M4d1 = 33, M4d2 = 31, M4pwm = 3; //M4 is the fourth motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
float M1p, M2p, M3p, M4p;

int x = 0;
int y = 0;
int r = 128;
int ref = 128;
int factor = 1;
int xpwm, ypwm;
int clawPos=90;
int rackPos=0;

Stepper clawStepper(clawStepperSteps, clawStepperP1, clawStepperP2);
Stepper baseStepper(baseStepperSteps, baseStepperP1, baseStepperP2);


void fwd();
void bwd();
void pwmcalc();
void dirset();
void loco(int M1pwm, int M2pwm, int M3pwm, int M4pwm, int M1p, int M2p, int M3p, int M4p);
void rotate();

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  ps2x.config_gamepad(PS2_clk, PS2_cmd, PS2_att, PS2_data, false, false);
  clawStepper.setSpeed(1000);
  baseStepper.setSpeed(60);
  claw.attach(45);//7
  rack1.attach(22);
  rack2.attach(24);
  pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, LOW);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, LOW);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, LOW);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, LOW);
}

void loop() {
  // put your main code here, to run repeatedly:
  ps2x.read_gamepad();


 y =  ps2x.Analog(PSS_RY); //Right Stick Up and Down
  x =  ps2x.Analog(PSS_RX); //Right Stick Left and Right
  r =  ps2x.Analog(PSS_LX); //Left Stick Left and Right
 
  if(ps2x.ButtonPressed(PSB_TRIANGLE))
  {
    clawStepper.step(clawStepperSteps*20);
    Serial.println("triangle"); 
  }
  if(ps2x.ButtonPressed(PSB_CROSS))
  {
    clawStepper.step(-clawStepperSteps*20);
    Serial.println("cross");
  }
  if(ps2x.Button(PSB_PAD_RIGHT))
  {
    baseStepper.step(-5);
  }
  if(ps2x.Button(PSB_PAD_LEFT))
  {
    baseStepper.step(5);
  }
 
  if(ps2x.ButtonPressed(PSB_CIRCLE))
  {
    Serial.println("circle");
    if(clawPos==90)
    {
      clawPos=180;
    }
    else if(clawPos==180)
    {
      clawPos=90;
    }
    claw.write(clawPos); 
  }
  if(ps2x.ButtonPressed(PSB_SQUARE))
  {
    Serial.println("sqaure");
    if(rackPos==0)
    {
      rackPos=180;
    }
    else if(rackPos==180)
    {
      rackPos=0;
    }
    rack1.write(rackPos);
    rack2.write(rackPos);
  }

  if (ps2x.Button(PSB_R3))
  {
    digitalWrite(M1d1, HIGH);
    digitalWrite(M1d2, HIGH);
    analogWrite(M1pwm, 255);
    digitalWrite(M2d1, HIGH);
    digitalWrite(M2d2, HIGH);
    analogWrite(M2pwm, 255);
    digitalWrite(M3d1, HIGH);
    digitalWrite(M3d2, HIGH);
    analogWrite(M3pwm, 255);
    digitalWrite(M4d1, HIGH);
    digitalWrite(M4d2, HIGH);
    analogWrite(M4pwm, 255);
  }
  else
  {
    pwmcalc();
    dirset();
  }
} 
  void rotation()
  {
    r = (r - 128) * factor;
    M1p =  M1p + r;
    M2p =  M2p - r;
    M3p =  M3p + r ;
    M4p =  M4p - r;
  }

  void loco(int M1pwm, int M2pwm, int M3pwm, int M4pwm, int M1p, int M2p, int M3p, int M4p)
  {
    analogWrite(M1pwm, abs(M1p));
    analogWrite(M2pwm, abs(M2p));
    analogWrite(M3pwm, abs(M3p));
    analogWrite(M4pwm, abs(M4p));
  }

  void soft_stop(int d1, int d2)
  {
    digitalWrite(d1, LOW);
    digitalWrite(d2, LOW);
  }
  void fwd(int d1, int d2)
  {
    digitalWrite(d1, HIGH);
    digitalWrite(d2, LOW);
  }

  void bwd(int d1, int d2)
  {
    digitalWrite(d1, LOW);
    digitalWrite(d2, HIGH);

  }

  void pwmcalc()
 {
    xpwm = (x - ref) * factor;
    ypwm = (ref - y) * factor;
    M3p = -xpwm * 0.707106781 + ypwm * 0.707106781;
    M4p = ypwm * 0.707106781 + xpwm * 0.707106781;
    M1p = M4p;
    M2p = M3p;
    if (r > 135 || r < 121) {
      rotation  ();
    }
    if (M1p > 255) M1p = 255;
    if (M2p > 255) M2p = 255;
    if (M3p > 255) M3p = 255;
    if (M4p > 255) M4p = 255;
    if (M1p < -255) M1p = -255;
    if (M2p < -255) M2p = -255;
    if (M3p < -255) M3p = -255;
    if (M4p < -255) M4p = -255;
  
    loco(M1pwm, M2pwm, M3pwm, M4pwm, M1p, M2p, M3p, M4p);
  }

  void dirset()
  {
    if (M1p > 10)
    {
      fwd(M1d1, M1d2);
      Serial.println("M1 F");
    }
    else if (M1p < 10 && M1p > -10) soft_stop(M1d1, M1d2);
    else {
      bwd(M1d1, M1d2);
      Serial.println("M1 B");
    }

    if (M2p > 10)
    {
      fwd(M2d1, M2d2);
      Serial.println("M2 F");
    }
    else if (M2p < 10 && M2p > -10) soft_stop(M2d1, M2d2);
    else {
      bwd(M2d1, M2d2);
      Serial.println("M2 B");
    }

    if (M3p > 10)
    {
      fwd(M3d1, M3d2);
      Serial.println("M3 F");
    }
    else if (M3p < 10 && M3p > -10) soft_stop(M3d1, M3d2);
    else {
      bwd(M3d1, M3d2);
      Serial.println("M3 B");
    }

    if (M4p > 10)
    {
      fwd(M4d1, M4d2);
      Serial.println("M4 F");
    }
    else if (M4p < 10 && M4p > -10) soft_stop(M4d1, M4d2);
    else {
      bwd(M4d1, M4d2);
      Serial.println("M4 B");
    }
  }

