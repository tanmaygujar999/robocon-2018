#include <PS2X_lib.h>
#include <Servo.h>
#include <Stepper.h>

PS2X ps2x;

Servo claw;
Servo rack1,rack2;

#define baseStepperP1 21
#define baseStepperP2 20
#define clawStepperP1 47
#define clawStepperP2 49
#define rackServo1 22
#define rackServo2 24
#define PS2_data 17
#define PS2_clk 46
#define PS2_cmd 50
#define PS2_att 48 
#define clawStepperSteps 1000
#define baseStepperSteps 800

int clawPos=90;
int rackPos=0;

Stepper clawStepper(clawStepperSteps, clawStepperP1, clawStepperP2);
Stepper baseStepper(baseStepperSteps, baseStepperP1, baseStepperP2);

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  ps2x.config_gamepad(PS2_clk, PS2_cmd, PS2_att, PS2_data, false, false);
  clawStepper.setSpeed(1000);
  baseStepper.setSpeed(60);
  claw.attach(45);//7
  rack1.attach(22);
  rack2.attach(24);
}

void loop() {
  // put your main code here, to run repeatedly:
  ps2x.read_gamepad();
 // ry =  ps2x.Analog(PSS_RY); //Right Stick Up and Down
 // rx =  ps2x.Analog(PSS_RX); //Right Stick Left and Right

  if(ps2x.ButtonPressed(PSB_TRIANGLE))
  {
    clawStepper.step(clawStepperSteps*20);
    Serial.println("triangle"); 
  }
  if(ps2x.ButtonPressed(PSB_CROSS))
  {
    clawStepper.step(-clawStepperSteps*20);
    Serial.println("cross");
  }
  if(ps2x.Button(PSB_PAD_RIGHT))
  {
    baseStepper.step(-5);
  }
  if(ps2x.Button(PSB_PAD_LEFT))
  {
    baseStepper.step(5);
  }
 
  if(ps2x.ButtonPressed(PSB_CIRCLE))
  {
    Serial.println("circle");
    if(clawPos==90)
    {
      clawPos=180;
    }
    else if(clawPos==180)
    {
      clawPos=90;
    }
    claw.write(clawPos); 
  }
  if(ps2x.ButtonPressed(PSB_SQUARE))
  {
    Serial.println("sqaure");
    if(rackPos==0)
    {
      rackPos=180;
    }
    else if(rackPos==180)
    {
      rackPos=0;
    }
    rack1.write(rackPos);
    rack2.write(rackPos);
  }
 }
