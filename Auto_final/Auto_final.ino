int M1d1 = 25, M1d2 = 23, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(23,25 respectively), and pwm is for the pwm pin(40)
int M2d1 = 27, M2d2 = 29, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(29,27 respectively), and pwm is for the pwm pin(38)
int M3d1 = 31, M3d2 = 33, M3pwm = 3; //M3 is the thirdmotor, d1,d2 are the direction pins(31,33 respectively), and pwm is for the pwm pin(44)
int M4d1 = 37, M4d2 = 35, M4pwm = 2; //M4 is the fourth motor, d1,d2 are the direction pins(37,35 respectively), and pwm is for the pwm pin(42)
float M1p, M2p, M3p, M4p;

int MaxSetSpeed=100;
class pController
{
  public:
  int setpoint,kp,output;
  int correction(int input)
  {
    output=(input-setpoint)*kp;
    return output;
  }
};
class Encoder
{
  public:
    int feedbackVel,t,intPin,dirPin,feedbackPwm;
    volatile long count=0,prev_count=0;
    
    Encoder(int pin1,int pin2)
    {
      t=millis(); 
      intPin=pin1;
      dirPin=pin2;
    }
    void calFeedbackVel()
    {
       if((t-millis())>=250)
       {
        feedbackVel=(count-prev_count)*t;
        prev_count=count;
        t=millis();
       }
    }
    void resetCount()
    {
      count=0;
    }
    void detCount()
    {
      if(dirPin)
      {
        count++;
      }
      else count--;
    }
    void Vel2Pwm()
    {
      feedbackPwm=map(feedbackVel,0,6.67,0,255);
    }
};


class Motor : public Encoder,public pController
{
  public:
  int dir1,dir2,inputVel,inputPwm,pwmPin;
  
  Motor(int pin1,int pin2,int pwm)
  {
    dir1=pin1;
    dir2=pin2
    pwmPin=pwm;
    digitalWrite(dir1,LOW);
    digitalWrite(dir2,LOW);
    analogWrite(pwmPin,LOW);
  }

  void setdirspeed()
  {
    inputPwm=abs(inputVel);
    if(inputVel>0)
    {
      digitalWrite(dir1,HIGH);
      digitalWrite(dir2,LOW);  
    }
    else 
    {
      digitalWrite(dir1,LOW);
      digitalWrite(dir2,HIGH);
    }
    analogWrite(pwmPin,inputPwm);
  }
  
};

void calcInputVel(Motor &M1,Motor &M2,Motor &M3,Motor &M4,vx,vy,rot,int length=4)
{
  int tfMatrix[4][3] = {{-0.707,0.707,1},{-0.707,-0.707,1},{0.707,-0.707,1},{0.707,0.707,1}};
  float calcdVel[4]={0,0,0,0};
  int inputVel[]={vx,vy,rot};
  for(int i=0;i<length;i++)
  {
    for(int j=0;j<(length-1);j++)
    {
      calcdVel[i]+=tfMatrix[i][j]*inputVel[j];
    }
  }
  M1.inputVel=calcdVel[0];
  M2.inputVel=calcdVel[1];
  M3.inputVel=calcdVel[2];
  M4.inputVel=calcdVel[3];
}

class

void setup() {
  // put your setup code here, to run once:
  Motor M1,M2,M3,M4;
  Serial.begin(9600);

}

void loop() {
  // put your main code here, to run repeatedly:
  
}
