/*
colour sensor code-Abhijit
place a black object first to calibrate the max value of sensor
an external button is suggested to calibrate the sensor

*/
//declare these globally
#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8

int frequency = 0;
int s=200;
void setup() {
  Serial.begin(9600);
  delay(1000);
  setupSensor();
}

void loop() {
  int d=detectColour();
  Serial.println("The detected colour is ");
  Serial.println(d);
  delay(2000);
}

int readred()
{
  // Setting red filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  // Printing the value on the serial monitor
  Serial.println("R= ");//printing name
  Serial.print(frequency);//printing RED color frequency
  Serial.print("  ");
  //delay(100);
  return frequency;
}
int readgreen()
{
  // Setting Green filtered photodiodes to be read
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  // Printing the value on the serial monitor
  Serial.print("G= ");//printing name
  Serial.print(frequency);//printing RED color frequency
  Serial.print("  ");
  //delay(100);
  return frequency;
}
int readblue()
{
  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  // Printing the value on the serial monitor
  Serial.print("B= ");//printing name
  Serial.print(frequency);//printing RED color frequency
  Serial.println("  ");
  //delay(100);
  return frequency;
}
void setSensorFrequency()
{
  // Setting frequency-scaling to 20%
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);
}
void setSensorPins()
{
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
}
int finalColour()
{
  int r=readred();
  int g=readgreen();
  int b=readblue();
  int colour=(r+g+b)/3;
  Serial.println("The Colour value is ");
  Serial.print(colour);
  return colour;
}
int detectColour()
{
  int f=finalColour();
  if(f<=int(s/3))
  return 0;
  else if(f>=int(s/3) && f<=int(2*s/3))
  return 1;
  else if(f>=int(2*s/3))
  return 2;
}
int calibrateSensor()
{
  Serial.print("Please place a black object in front of the sensor for 10 sec");
  delay(5000);
  s=finalColour();
  delay(5000);
  Serial.println("The max value of sensor is ");
  Serial.print(s);
  delay(5000);
}
void setupSensor()
{
  setSensorPins();
  setSensorFrequency();
  calibrateSensor();
}
