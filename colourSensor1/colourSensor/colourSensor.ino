#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8
int frequency = 0;
int sumOfSquare_r=0;
void setup() {
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
  
  // Setting frequency-scaling to 20%
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);
  
  Serial.begin(9600);
}

void loop() {
  int count = 0;
  while(count<50){
  // Setting red filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  // Printing the value on the serial monitor
  Serial.println("R= ");//printing name
  Serial.print(frequency);//printing RED color frequency
  Serial.print("  ");
  
  delay(1);
  count++;
  }
  /*
  // Setting Green filtered photodiodes to be read
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  // Printing the value on the serial monitor
  //Serial.print("G= ");//printing name
  //Serial.print(frequency);//printing RED color frequency
  //Serial.print("  ");
  
  delay(1);

  // Setting Blue filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
  // Reading the output frequency
  frequency = pulseIn(sensorOut, LOW);
  // Printing the value on the serial monitor
  //Serial.print("B= ");//printing name
  //Serial.print(frequency);//printing RED color frequency
  //Serial.println("  ");
  
  delay(1);
  */
  
  int a=detectColour(rms(meanSquare(frequency)));
  Serial.println(a);
  delay(2000);
}
int detectColour(int r)
{
  if(r<50)
  return 0;
  else if(r<100)
  return 1;
  else if(r>100)
  return 2;
  int sumOfSquare_r=0;
}
int meanSquare(int r)
{
  
  sumOfSquare_r += r*r;
  return sumOfSquare_r;
}
int rms(int a)
{
  int mean = a/50;
  return sqrt(mean);
}
