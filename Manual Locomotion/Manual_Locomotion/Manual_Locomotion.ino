#include <PS2X_lib.h>

PS2X ps2x;

int M1d1 = 23, M1d2 = 25, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M2d1 = 27, M2d2 = 29, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M3d1 = 33, M3d2 = 31, M3pwm = 3; //M3 is the thirdmotor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
int M4d1 = 35, M4d2 = 37, M4pwm = 2; //M4 is the fourth motor, d1,d2 are the direction pins(2,6 respectively), and pwm is for the pwm pin(4)
float M1p, M2p, M3p, M4p;

int PlyStnRStickUpDn = 0; //Value read off the PS2 Right Stick up/down.
int PlyStnRStickLtRt = 0; //Value read off the PS2 Right Stick left/right
int PlyStnLStickUpDn = 0; //Value read off the PS2 Left Stick up/down
int PlyStnLStickLtRt = 0; // Value read off the PS2 Left Stick left/right

int data = 17;
int clk = 46;
int cmd = 50;
int att = 48;
int x = 0;
int y = 0;
int r = 128;
int ref = 128;
int factor = 2.5;
int xpwm, ypwm;

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  ps2x.config_gamepad(clk, cmd, att, data, false, false);
  pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, LOW);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, LOW);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, LOW);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, LOW);
}

void fwd();
void bwd();
void pwmcalc();
void dirset();
void move(int M1pwm, int M2pwm, int M3pwm, int M4pwm, int M1p, int M2p, int M3p, int M4p);
void rotate();
void loop() {
  // put your main code here, to run repeatedly:

  ps2x.read_gamepad();

  y =  ps2x.Analog(PSS_RY); //Right Stick Up and Down
  x =  ps2x.Analog(PSS_RX); //Right Stick Left and Right
  r =  ps2x.Analog(PSS_LX); //Left Stick Left and Right
  PlyStnLStickUpDn = ps2x.Analog(PSS_LY); //left Stick Up and Down
  //Serial.println(r);
  /*Serial.println(x);
    Serial.println("/////////////////////////////////////////");
    Serial.println(abs(M1p));
    Serial.println(abs(M2p));
    Serial.println(abs(M3p));
    Serial.println(abs(M4p));
    Serial.println("========================================");
  */
  pwmcalc();
  dirset();
  delay(1000);
  Serial.println(r);
  Serial.println(abs(M1p));
  Serial.println(abs(M2p));
  Serial.println(abs(M3p));
  Serial.println(abs(M4p));

}

void rotation()
{
  
  //if (r < 64)
  //r = 64;
  //if (r > 192)
  // r = 192;
   
  r = (r - 128) * factor;
  M1p =  M1p + r;
  M2p =  M2p - r;
  M3p =  M3p + r ;
  M4p =  M4p - r;
}



void move(int M1pwm, int M2pwm, int M3pwm, int M4pwm, int M1p, int M2p, int M3p, int M4p)
{
  analogWrite(M1pwm, abs(M1p));
  analogWrite(M2pwm, abs(M2p));
  analogWrite(M3pwm, abs(M3p));
  analogWrite(M4pwm, abs(M4p));
}

void soft_stop(int d1, int d2)
{
  digitalWrite(d1, LOW);
  digitalWrite(d2, LOW);
}
void fwd(int d1, int d2)
{
  digitalWrite(d1, HIGH);
  digitalWrite(d2, LOW);
}

void bwd(int d1, int d2)
{
  digitalWrite(d1, LOW);
  digitalWrite(d2, HIGH);

}

void pwmcalc()
{
  xpwm = (x - ref) * factor;
  ypwm = (ref - y) * factor;
  M3p = xpwm * 0.707106781 + ypwm * 0.707106781;
  M4p = ypwm * 0.707106781 - xpwm * 0.707106781;
  M1p = M4p;
  M2p = M3p;
  if (r > 135|| r < 121) {
    rotation  ();
  }
  if (M1p > 255) M1p = 255;
  if (M2p > 255) M2p = 255;
  if (M3p > 255) M3p = 255;
  if (M4p > 255) M4p = 255;
  if (M1p < -255) M1p = -255;
  if (M2p < -255) M2p = -255;
  if (M3p < -255) M3p = -255;
  if (M4p < -255) M4p = -255;
  
  move(M1pwm, M2pwm, M3pwm, M4pwm, M1p, M2p, M3p, M4p);
}

void dirset()
{
  if (M1p > 10)
  {
    fwd(M1d1, M1d2);
    Serial.println("M1 F");
  }
  else if (M1p < 10 && M1p > -10) soft_stop(M1d1, M1d2);
  else {
    bwd(M1d1, M1d2);
    Serial.println("M1 B");
  }

  if (M2p > 10)
  {
    fwd(M2d1, M2d2);
    Serial.println("M2 F");
  }
  else if (M2p < 10 && M2p > -10) soft_stop(M2d1, M2d2);
  else {
    bwd(M2d1, M2d2);
    Serial.println("M2 B");
  }

  if (M3p > 10)
  {
    fwd(M3d1, M3d2);
    Serial.println("M3 F");
  }
  else if (M3p < 10 && M3p > -10) soft_stop(M3d1, M3d2);
  else {
    bwd(M3d1, M3d2);
    Serial.println("M3 B");
  }

  if (M4p > 10)
  {
    fwd(M4d1, M4d2);
    Serial.println("M4 F");
  }
  else if (M4p < 10 && M4p > -10) soft_stop(M4d1, M4d2);
  else {
    bwd(M4d1, M4d2);
    Serial.println("M4 B");
  }

}


/*{
  /*void forward()
  {
  clockwise(M1d1, M1d2, M1pwm); //Rotating the fourth wheel clockwise
  clockwise(M2d1, M2d2, M2pwm); //Rotating the fourth wheel clockwise
  clockwise(M3d1, M3d2, M3pwm); //Rotating the second wheel anticlockwise
  clockwise(M4d1, M4d2, M4pwm); //Rotating the second wheel anticlockwise
  // delay(500);
  }

  void backward()
  {
  anticlockwise(M1d1, M1d2, M1pwm); //Rotating the fourth wheel anticlockwise
  anticlockwise(M2d1, M2d2, M2pwm); //Rotating the fourth wheel anticlockwise
  anticlockwise(M3d1, M3d2, M3pwm); //Rotating the second wheel clockwise
  anticlockwise(M4d1, M4d2, M4pwm); //Rotating the second wheel clockwise
  //delay(500);
  }

  void right()
  {
  anticlockwise(M1d1, M1d2, M1pwm); //Rotating the first wheel anitclockwise
  clockwise(M2d1, M2d2, M2pwm); //Rotating the second wheel clockwise
  clockwise(M3d1, M3d2, M3pwm); //Rotating the third wheel clockwise
   anticlockwise(M4d1, M4d2, M4pwm); //Rotating the fourth wheel anticlockwise
  //  delay(500);
  }

  void left()
  {
  clockwise(M1d1, M1d2, M1pwm); //Rotating the first wheel clockwise
  anticlockwise(M2d1, M2d2, M2pwm); //Rotating the second wheel anticlockwise
  anticlockwise(M3d1, M3d2, M3pwm); //Rotating the third wheel anticlockwise
  clockwise(M4d1, M4d2, M4pwm); //Rotating the fourth wheel clockwise
  //  delay(500);
  }

  void forwardright()
  { //We will stop Motors 1 and 3
  clockwise(M2d1, M2d2, M2pwm); //Rotating the second wheel anticlockwise
  anticlockwise(M4d1, M4d2, M4pwm); //Rotating the fourth wheel anticlockwise
  // delay(500);
  }

  void forwardleft()
  { //We will stop Motors 2 and 4
  clockwise(M1d1, M1d2, M1pwm); //Rotating the first wheel clockwise
  anticlockwise(M3d1, M3d2, M3pwm); //Rotating the third wheel clockwise
  //delay(500);
  }

  void backwardright()
  { //We will stop Motors 2 and 4
  anticlockwise(M1d1, M1d2, M1pwm); //Rotating the first wheel anticlockwise
  clockwise(M3d1, M3d2, M3pwm); //Rotating the third wheel anticlockwise
  //delay(500);
  }

  void backwardleft()
  { //We will stop Motors 1 and 3
  anticlockwise(M2d1, M2d2, M2pwm); //Rotating the second wheel clockwise
  clockwise(M4d1, M4d2, M4pwm); //Rotating the fourth wheel clockwise
  //  delay(500);
  }*/

