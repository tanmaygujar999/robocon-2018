#define Kp 2.5
#define Kd 2.5
#define MaxSpeed 240

int lsa_in = A14;
int setpoint = 33;
int error = 0;



int sensorVal = 0;
int lastError = 0;
int PwmToMotors = 0;
int LeftMotorSpeed = 0;
int RightMotorSpeed = 0;
int MaxSetSpeed = 70;
int M1d1 = 23, M1d2 = 25, M1pwm = 4; //M1 is the first motor, d1,d2 are the direction pins(23,25 respectively), and pwm is for the pwm pin(40)
int M2d1 = 29, M2d2 = 27, M2pwm = 5; //M2 is the second motor, d1,d2 are the direction pins(29,27 respectively), and pwm is for the pwm pin(38)
int M3d1 = 31, M3d2 = 33, M3pwm = 3; //M3 is the thirdmotor, d1,d2 are the direction pins(31,33 respectively), and pwm is for the pwm pin(44)
int M4d1 = 37, M4d2 = 35, M4pwm = 2; //M4 is the fourth motor, d1,d2 are the direction pins(37,35 respectively), and pwm is for the pwm pin(42)
float M1p, M2p, M3p, M4p;

void setup()
{
  Serial.begin(9600);

  pinMode(M1d1, OUTPUT);
  pinMode(M1d2, OUTPUT);
  pinMode(M1pwm, OUTPUT);
  pinMode(M2d1, OUTPUT);
  pinMode(M2d2, OUTPUT);
  pinMode(M2pwm, OUTPUT);
  pinMode(M3d1, OUTPUT);
  pinMode(M3d2, OUTPUT);
  pinMode(M3pwm, OUTPUT);
  pinMode(M4d1, OUTPUT);
  pinMode(M4d2, OUTPUT);
  pinMode(M4pwm, OUTPUT);

  digitalWrite(M1d1, LOW);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, LOW);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, LOW);
  analogWrite(M2pwm, LOW);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, LOW);
  analogWrite(M3pwm, LOW);
  digitalWrite(M4d1, LOW);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, LOW);
}
void forward(int leftPwm, int rightPwm)
{
  digitalWrite(M1d1, HIGH);
  digitalWrite(M1d2, LOW);
  analogWrite(M1pwm, leftPwm);
  digitalWrite(M2d1, LOW);
  digitalWrite(M2d2, HIGH);
  analogWrite(M2pwm,rightPwm);
  digitalWrite(M3d1, LOW);
  digitalWrite(M3d2, HIGH);
  analogWrite(M3pwm, rightPwm);
  digitalWrite(M4d1, HIGH);
  digitalWrite(M4d2, LOW);
  analogWrite(M4pwm, leftPwm);
}

void loop()
{
  sensorVal = float(analogRead(lsa_in)) * 0.076;
  error = sensorVal - setpoint;
  PwmToMotors = float(Kp * error + Kd * (error - lastError));
  lastError = error;
  if(lastError < 40)
  {
    LeftMotorSpeed = MaxSetSpeed + PwmToMotors;
    RightMotorSpeed = MaxSetSpeed - PwmToMotors;
    if (LeftMotorSpeed > MaxSpeed) LeftMotorSpeed = MaxSpeed;
    if (RightMotorSpeed > MaxSpeed) RightMotorSpeed = MaxSpeed;
    if (LeftMotorSpeed < 0)LeftMotorSpeed = 0;
    if (RightMotorSpeed < 0)RightMotorSpeed = 0;
  }
  else
  {
    LeftMotorSpeed = 70;
    RightMotorSpeed = 7+0;
  }
  Serial.print(sensorVal);
  Serial.print("  ");
  Serial.print(LeftMotorSpeed);
  Serial.print("  ");
  Serial.print(RightMotorSpeed);
  Serial.print("  ");
  Serial.print(lastError);
  Serial.println();
  forward(LeftMotorSpeed, RightMotorSpeed);
}
